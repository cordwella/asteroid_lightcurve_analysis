import numpy as np

from scipy.stats.distributions import chi2


# look all of this needs a proper look over tomorrow

def extract_best_fit(data_dict, check_doubles=True):
    """
    check doubles: ensure that it is double peaked
    """

    f_b_i, g_b_i = np.unravel_index(data_dict['output_chi_squared'].argmin(),
                                    data_dict['output_chi_squared'].shape)

    chi_min = np.min(data_dict['output_chi_squared'])

    best_f = data_dict['f_values'][f_b_i]
    best_g = data_dict['g_values'][g_b_i]

    data_dict['f_b_i'] = f_b_i
    data_dict['g_b_i'] = g_b_i

    is_double_peaked, amplitude = get_amplitude(data_dict)

    data_dict['force_double'] = False

    if check_doubles: # and not is_double_peaked:
        data_dict['force_double'] = True

        # attempt to get a double peaked fit by doulbing the
        # frequency and checking for the minimum in its vicinty
        # check that the output chi squared is less than twice or something
        # of the double peaked check

        double_freq = best_f * 0.5
        # find closest matches to this inside f_values
        # let a few closest matches to check within the likely uncertainty
        # ranges for the twice peak

        # closest match, find lowest chi squared
        closest_match = np.argmin(np.abs(data_dict['f_values'] - double_freq))
        restricted_chi_field = data_dict['output_chi_squared'][
            closest_match - 10:closest_match + 10, :]

        try:
            f_b_i, g_b_i = np.unravel_index(restricted_chi_field.argmin(),
                                            restricted_chi_field.shape)
            new_min = np.min(restricted_chi_field)
            f_b_i = f_b_i + closest_match - 10

            #if new_min >= 2 * chi_min: # chi2.ppf(0.97, df=data_dict['dof']) + chi_min:
            if new_min >= 3 * chi_min: # chi2.ppf(0.97, df=data_dict['dof']) + chi_min:
                # reject
                print("Rejecting double frequency solution")
            else:
                data_dict['f_b_i'] = f_b_i
                data_dict['g_b_i'] = g_b_i

                is_double_peaked, amplitude = get_amplitude(data_dict)
        except ValueError:
            print("Rejecting double frequency solution")


    # TODO: amplitude uncertainty?
    data_dict['amplitude'] = amplitude
    # uncertainties in all values and summary blah

    data_dict['min_red_chi'] = \
        data_dict['output_chi_squared'][f_b_i][g_b_i]/data_dict['dof']

    data_dict['h_uncertainty_limits'], data_dict['g_uncertainty_limits'] =\
        uncertain_in_h_g(data_dict)
    data_dict['f_uncertainty_limits'] = uncertain_in_f(data_dict)
    data_dict['lin_param_covariances'] = data_dict[
        'output_linear_covarainces'][f_b_i][g_b_i]
    return data_dict


def get_amplitude(data_dict):
    """ Get the amplitude of a light curve based on its fourier fit

    returns:
    True/ False: True for double peaked, false for single peaked
    Amplitude in magnitudes, as half (max - min)
    """

    f_b_i, g_b_i = data_dict['f_b_i'], data_dict['g_b_i']

    fourier_coeffs = (data_dict['output_linear_parameters'][f_b_i][g_b_i])

    t_span = np.linspace(0, 1, 50)

    theta = 2 * np.pi * t_span

    model_prediction = (fourier_coeffs[0] + fourier_coeffs[1] * np.sin(theta)
                        + fourier_coeffs[2] * np.cos(theta)
                        + fourier_coeffs[3] * np.sin(2 * theta) +
                        fourier_coeffs[4] * np.cos(2 * theta))

    b = (np.diff(np.sign(np.diff(model_prediction))) > 0).nonzero()[0] + 1 # local min
    c = (np.diff(np.sign(np.diff(model_prediction))) < 0).nonzero()[0] + 1 # local max
    # is amplitude max amplitude of min amplitude
    amplitude = 0.5 * (np.max(model_prediction[c])
                       - np.min(model_prediction[b]))

    if len(b) == 1:
        print('single peaked')
        return False, amplitude

    return True, amplitude


def uncertain_in_h_g(data_dict):
    """ Find the uncertainty in H due to the uncertainty in G """
    # get the minimum G value
    chi_2 = data_dict['output_chi_squared']
    fourier_coeffs = data_dict['output_linear_parameters']

    f_b_i = data_dict['f_b_i']
    g_b_i = data_dict['g_b_i']

    H = fourier_coeffs[f_b_i][g_b_i][0]

    g_chi_field = chi_2[f_b_i, :]
    chi_min = chi_2[f_b_i, g_b_i]

    one_sig_offset = chi2.ppf(0.67, df=data_dict['dof'])
    intercept = chi_min + one_sig_offset

    # In this case due to both the good sampling in G as well as the
    # single peaked nature of the chi field doing this is good enough

    if g_b_i == 0:
        # cannot get G uncertainty
        g_left_intercept = 0
        # gonna bullshit it

        g_right_intercept = np.argmin(
            np.abs((g_chi_field - intercept)[g_b_i:])) + g_b_i
        # get the one sigma uncertainties o that
        # find the g_i on the one sigma uncertainties

        g_upper = data_dict['g_values'][g_right_intercept]

        # find the best fit
        h_upper_g = fourier_coeffs[f_b_i][g_right_intercept][0]
        h_lower_g = fourier_coeffs[f_b_i][g_left_intercept][0]

        return ((H, h_lower_g, h_upper_g),
                (data_dict['g_values'][g_b_i],
                 data_dict['g_values'][g_b_i],
                 g_upper))

    g_left_intercept = np.argmin(np.abs((g_chi_field - intercept)[:g_b_i])) - 1
    g_right_intercept = np.argmin(np.abs((g_chi_field - intercept)[g_b_i:])) + g_b_i
    # get the one sigma uncertainties o that
    # find the g_i on the one sigma uncertainties

    g_lower = data_dict['g_values'][g_left_intercept]
    g_upper = data_dict['g_values'][g_right_intercept]

    # find the best fit
    h_upper_g = fourier_coeffs[f_b_i][g_right_intercept][0]
    h_lower_g = fourier_coeffs[f_b_i][g_left_intercept][0]

    return ((H, h_lower_g, h_upper_g),
            (data_dict['g_values'][g_b_i], g_lower, g_upper))


def uncertain_in_f(data_dict):
    f_b_i = data_dict['f_b_i']
    g_b_i = data_dict['g_b_i']
    dof = data_dict['dof']

    one_sig_offset = chi2.ppf(0.67, df=dof)
    chi_2 = data_dict['output_chi_squared'][:, g_b_i]
    chi_min = chi_2[f_b_i]

    print(chi_min)
    print(chi_min + one_sig_offset)
    print(chi_min/dof)
    print((chi_min + one_sig_offset)/dof)

    left_data = chi_2[:f_b_i][::-1]
    right_data = chi_2[f_b_i:]

    if f_b_i == 0:
        print("Unable to find right frequency uncertainty")
        data_dict['bad_f_uncertainty'] = True
        return (data_dict['f_values'][f_b_i], 0, 0)

    # find first element in left and right data > chi_min + one sig offset
    left_index = f_b_i - np.argmax(left_data >= chi_min + one_sig_offset) - 1

    if not (left_data >= chi_min + one_sig_offset).any():
        print("Unable to find left frequency uncertainty")
        data_dict['bad_f_uncertainty'] = True
    # print(chi_2[:f_b_i][::-1])

    right_index = np.argmax(right_data >= chi_min + one_sig_offset) + f_b_i

    if not (right_data >= chi_min + one_sig_offset).any():
        print("Unable to find right frequency uncertainty")
        data_dict['bad_f_uncertainty'] = True


    # can now either find the intercept between the two to get the actual
    # point where it would intercept, however im going to be lazy and not do
    # that right now

    right_one_sig = data_dict['f_values'][right_index]
    left_one_sig = data_dict['f_values'][left_index]

    data_dict['f_uncertain_indices'] = (left_index, right_index,
                                        left_one_sig, right_one_sig)

    data_dict['f_uncertain_indices'] = 0.5 * abs(right_one_sig - left_one_sig)
    return (data_dict['f_values'][f_b_i], right_one_sig, left_one_sig)

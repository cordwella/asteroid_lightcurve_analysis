import pickle
import pandas as pd
from matplotlib import pyplot as plt

# Identify lightcurves that may require jacknifing
# in this this means lightcurves with multiple seperate sections
# with enough points to be considered on their own

from pre_process import (adjust_for_distance, pre_clean_data,
                         update_error_estimate)
from chi_fit import generate_chi_squared_grid

asteroid_id = 2011


def run(asteroid_id,
        input_file_format='/home/amelia/Documents/minorplanets/backup/export_jul22/{}_out.csv',
        output_folder='jack_oct_9/_inc_err'):

    filename = input_file_format.format(asteroid_id)

    # general preprocessing
    data = adjust_for_distance(pd.read_csv(filename))
    data = data.dropna()
    update_error_estimate(data)

    plt.plot(data['adjusted_time'],
             data['distance_red_mag'], 'o')

    max_gap = 14  # in jd

    epoch_dates = []
    previous_time = 0

    for index, row in data.iterrows():
        current_time = row['adjusted_time']

        if current_time - max_gap > previous_time:
            epoch_dates.append(current_time)

        previous_time = current_time

    if len(epoch_dates) > 1:
        for i in range(len(epoch_dates)):
            print("Running epochs")
            output_file = "{}{}_ep{}.dat".format(output_folder, asteroid_id, i)
            # slice based on epoch indices
            print(i)
            if i + 1 < len(epoch_dates):
                # for times between the two starting at the first
                data_range = data[
                    (data['adjusted_time'] >= epoch_dates[i]) &
                    (data['adjusted_time'] < epoch_dates[i + 1])]
            else:
                data_range = data[data['adjusted_time'] >= epoch_dates[i]]

            # testing so we print and plot
            # remove outliers
            plt.plot(data_range['adjusted_time'],
                     data_range['distance_red_mag'], 'x',
                     label='start {}'.format(epoch_dates[i]))

            try:
                data_range = pre_clean_data(data_range)
            except Exception as e:
                print(e)
                print('Not enough data for this epoch')
                continue

            data_dict = generate_chi_squared_grid(data_range)

            pickle.dump(data_dict, open(output_file, "wb"))
    else:
        print("No need to run split lightcurves")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    run(2011)

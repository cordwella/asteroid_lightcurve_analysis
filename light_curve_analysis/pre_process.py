import pandas as pd
import numpy as np


def adjust_for_distance(data):
    """ Adjust sky magnitudes for the effect of distance"""

    corrected_mag = data['skymag'] - (5 * np.log10(data['delta'] * data['r']))

    data['distance_red_mag'] = corrected_mag
    return data


def pre_clean_data(data, threshold=2, overremoval=0.3):
    """
    Remove NaN values and outliners from the data table

    Outliers are found by fitting a linear model across values of alpha
    and then finding those that are greater than threshold standard deviations
    of the line

    input data: dataframe
    threshold: sigma level for point removal
    function fit: constant or linear

    overremoval: percentage that if more than that percent of values are
    removed will stop the attempt

    returns cleaned data
    """

    data = data.dropna()

    removed_points_last_round = True

    initial_data_length = len(data)

    while removed_points_last_round:
        current_len = len(data)
        # fit linear model and remove misfitting points

        # fit the linear model based on alpha and distanced_red_mag
        p = np.poly1d(np.polyfit(data['alpha'], data['distance_red_mag'], 1))

        # difference
        d = data['distance_red_mag'] - p(data['alpha'])

        data = data.drop(data[np.abs(d) > threshold * np.std(d)].index,
                         inplace=False)

        # repeat until no more points are removed or
        if len(data) < (1 - overremoval) * initial_data_length or len(data) < 20:
            raise Exception("Not enough data to continue")

        removed_points_last_round = (current_len != len(data))

    return data


def update_error_estimate(data):
    """ Update error estimate with the bullshit 390 way """
    # error correction
    data['skymagerr_fix'] = data['skymagerr'] * 3 + 0.14

from matplotlib import pyplot as plt
import pickle
import pandas as pd

from light_curve_analysis.extract_best_fits import extract_best_fit
from light_curve_analysis.output import print_summary, csv_summary, check_goodness
from light_curve_analysis.pre_process import adjust_for_distance, pre_clean_data
from light_curve_analysis import graphs

# %matplotlib notebook

asteroid_id = 2417

pickle_folder = 'full_run_390_error_fix/'
input_file_format = '/home/amelia/Documents/minorplanets/backup/august11/export/{}_out.csv'
image_output_folder = 'full_run_images/'

csv_output_file = 'full_run_images/summary.csv'

graphs.apply_styles()


def run_post_process(asteroid_id, pickle_folder, input_file_format, image_output_folder):
    data, data_dict = load_data(asteroid_id, pickle_folder, input_file_format)
    print_summary(data_dict, data)

    # display summary image
    # ask human advice? (possibly)
    # save summary image

    graphs.create_summary_plot(asteroid_id, data_dict, data)
    plt.subplots_adjust(top=0.92, wspace=0.25, hspace=0.25)

    with open(csv_output_file, 'a') as f:
        a = csv_summary(asteroid_id, data_dict, data)
        f.write(a + "\n")
    print(a)

    plt.savefig(image_output_folder + str(asteroid_id) + ".png")

    # save image


def load_data(asteroid_id, pickle_folder, input_file_format):
    filename = input_file_format.format(asteroid_id)

    # general preprocessing
    data = adjust_for_distance(pd.read_csv(filename))
    data = data.dropna()
    data = pre_clean_data(data)
    data['skymagerr_fix'] = (3 * data['skymagerr']) + 0.05

    output_file = "{}{}.p".format(pickle_folder, asteroid_id)

    data_dict = pickle.load(open(output_file, 'rb'))

    extract_best_fit(data_dict)
    check_goodness(data_dict, data)

    return data, data_dict


run_post_process(asteroid_id, pickle_folder, input_file_format, image_output_folder)
# plt.subplots_adjust(top=0.9, wspace=0.25, hspace=0.25

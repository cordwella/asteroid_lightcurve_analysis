import pickle
import pandas as pd

from pre_process import (adjust_for_distance, pre_clean_data,
                         update_error_estimate)
from chi_fit import generate_chi_squared_grid

import os


def run(asteroid_id,
        input_file_format='/home/amelia/Documents/minorplanets/backup/export_jul22/{}_out.csv',
        output_folder='test_with_error_add_005/'):
    """ Read data and compute chi field for full set"""

    filename = input_file_format.format(asteroid_id)

    # general preprocessing
    data = adjust_for_distance(pd.read_csv(filename))
    data = data.dropna()
    data = pre_clean_data(data)
    data['skymagerr_fix'] = data['skymagerr'] # (3 * data['skymagerr']) + 0.05

    data_dict = generate_chi_squared_grid(data)

    output_file = "{}{}.p".format(output_folder, asteroid_id)

    pickle.dump(data_dict, open(output_file, "wb"))


def run_folder(folder_name, output_folder):
    for f in os.listdir(folder_name):

        if f.split('.')[1] == 'csv':
            ast_id = f.split('_')[-2].split('/')[-1]

            output_file = output_folder + '{}.p'.format(ast_id)

            if os.path.isfile(output_file):
                print("Output already exists")
            else:
                try:
                    run(ast_id, folder_name + "{}_out.csv", output_folder)
                except Exception as e:
                    print(e)
                    f = open(output_folder + ast_id + "err.txt", 'w')
                    f.write(str(e))
                    f.close()


if __name__ == '__main__':
    # /home/amelia/Documents/minorplanets/backup/august11/export/157_out.csv

    output_folder = 'full_run_390_error_fix/'
    inptut_file_format = '{}'
    ast_list = []

    run_folder('/home/amelia/Documents/minorplanets/backup/august11/export/',
               'full_run_no_error_correction/')

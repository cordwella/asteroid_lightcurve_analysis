from light_curve_analysis.chi_fit import phase_function
from matplotlib import pyplot as plt
import numpy as np


def epoch_data(data, max_gap=7):

    max_gap = 14  # in jd

    epochs = []

    epoch_dates = []
    previous_time = 0

    for index, row in data.iterrows():
        current_time = row['adjusted_time']

        if current_time - max_gap > previous_time:
            epoch_dates.append(current_time)

        previous_time = current_time

    if len(epoch_dates) > 1:
        for i in range(len(epoch_dates)):
            if i + 1 < len(epoch_dates):
                # for times between the two starting at the first
                epochs.append(data[
                    (data['adjusted_time'] >= epoch_dates[i]) &
                    (data['adjusted_time'] < epoch_dates[i + 1])])
            else:
                epochs.append(data[data['adjusted_time'] >= epoch_dates[i]])
    else:
        return [data]
    return epochs


def plot_chi_field(data_dict):
    plt.imshow(data_dict['output_chi_squared']/data_dict['dof'])
    plt.ylabel("Frequency index")
    plt.xlabel("Phase value index")
    plt.colorbar()
    plt.title("Reduced $\Chi^2$ field")


def plot_best_fit(data_dict, data, use_epochs=True):
    # perhaps also plot uncertainty of model fit
    best_f = data_dict['f_uncertainty_limits'][0]
    best_g = data_dict['g_uncertainty_limits'][0]

    f_b_i, g_b_i = data_dict['f_b_i'], data_dict['g_b_i']
    fourier_coeffs = (data_dict['output_linear_parameters'][f_b_i][g_b_i])

    period = 1/best_f

    epochs = [data]
    zero_time = data['adjusted_time'].iloc[0]
    if use_epochs:
        epochs = epoch_data(data)

    for e in epochs:
        plt.errorbar(((e['adjusted_time'] - zero_time) % period)/period,
                     e['distance_red_mag'].to_numpy() +
                     phase_function(best_g, e['alpha'].to_numpy()),
                     e['skymagerr_fix'], fmt='o', label="Observations")

    # model light curve
    t_span = np.linspace(0, 1, 100)

    theta = 2 * np.pi * t_span

    model_prediction = (fourier_coeffs[0] + fourier_coeffs[1] * np.sin(theta)
                        + fourier_coeffs[2] * np.cos(theta)
                        + fourier_coeffs[3] * np.sin(2 * theta) +
                        fourier_coeffs[4] * np.cos(2 * theta))

    plt.plot(t_span, model_prediction, label="Fitted model lightcurve",
             color='#888')
    plt.title("Timewrapped observation \n "
              "Period {:6.4} h".format(24/best_f))
    # plt.legend()
    plt.xlabel("Portion of period")
    plt.ylabel("Reduced I Magnitude")

    plt.gca().invert_yaxis()


def plot_periodogram(data_dict, data):
    f_b_i = data_dict['f_b_i']
    g_b_i = data_dict['g_b_i']

    plt.axvline(data_dict['f_uncertainty_limits'][1], color="#999",
                label='One sigma uncertainty')
    plt.axvline(data_dict['f_uncertainty_limits'][2], color="#999")

    plt.axvline(data_dict['f_values'][f_b_i], color="#222", linestyle='--',
                label='Best frequency fit')

    plt.plot(data_dict['f_values'],
             data_dict['output_chi_squared'][:, g_b_i]/data_dict['dof'])

    plt.xlabel("Frequency jd$^{-1}$")
    plt.ylabel("Normalised $\chi^2$")

    # plt.legend()
    plt.title("$\\chi^2$ field at best phase fit")


def plot_full_lightcurve(data, use_epochs=True):
    epochs = [data]
    zero_time = data['adjusted_time'].iloc[0]
    if use_epochs:
        epochs = epoch_data(data)

    for e in epochs:
        plt.errorbar(e['adjusted_time'] - zero_time,
                     #data['distance_red_mag'],
                     e['skymag'],
                     e['skymagerr_fix'], fmt='o', label="Observations")

    plt.gca().invert_yaxis()
    # model light curve
    plt.title("Observered Lightcurve")
    # plt.legend()
    plt.xlabel("Date (jd)")
    # plt.ylabel("Distance Reduced I Magnitude")
    plt.ylabel("I Magnitude")


def plot_adjusted_phase_relationship(data_dict, data, use_epochs=True):
    """ Plot the data after subtraction of fourier terms to clearly show
    the phase relationship """

    best_f = data_dict['f_uncertainty_limits'][0]
    best_g = data_dict['g_uncertainty_limits'][0]

    f_b_i, g_b_i = data_dict['f_b_i'], data_dict['g_b_i']
    fourier_coeffs = (data_dict['output_linear_parameters'][f_b_i][g_b_i])

    # model light curve
    a_span = np.linspace(np.min(data['alpha']), np.max(data['alpha']))

    period = 1/best_f

    model_g = fourier_coeffs[0] - phase_function(best_g, a_span)

    # this should have error bars
    epochs = [data]
    zero_time = data['adjusted_time'].iloc[0]
    if use_epochs:
        epochs = epoch_data(data)

    for e in epochs:
        theta = (((e['adjusted_time'] - zero_time))/period) * 2 * np.pi
        theta = 2 * np.pi * ( (e['adjusted_time'] - zero_time) % period)/period

        model_prediction = (fourier_coeffs[1] * np.sin(theta)
                            + fourier_coeffs[2] * np.cos(theta)
                            + fourier_coeffs[3] * np.sin(2 * theta)
                            + fourier_coeffs[4] * np.cos(2 * theta))

        adjusted_lightcurve = e['distance_red_mag'] - model_prediction

        plt.errorbar(e['alpha'], adjusted_lightcurve,
                     e['skymagerr_fix'], fmt='o')

    plt.plot(a_span, model_g, label='Model Phase Curve', color='#888')

    plt.gca().invert_yaxis()

    plt.title("Phase Relationship")
    # plt.legend()
    plt.xlabel("Phase angle $\\alpha$")
    plt.ylabel("Reduced I Magnitude")


def create_summary_plot(asteroid_id, data_dict, data, use_epochs=True):
    fig, axes = plt.subplots(2, 2, figsize=(7.5, 6.5))#figsize=(14.5, 11))
    fig.suptitle('Asteroid {}'.format(asteroid_id))

    plt.axes(axes[0, 0])
    plot_best_fit(data_dict, data, use_epochs)
    plt.axes(axes[0, 1])
    plot_periodogram(data_dict, data)
    plt.axes(axes[1, 0])
    plot_adjusted_phase_relationship(data_dict, data, use_epochs)
    plt.axes(axes[1, 1])
    plot_full_lightcurve(data, use_epochs)
    plt.subplots_adjust(top=0.90, wspace=0.25, hspace=0.25)


def apply_styles():
    # im aware this code is bad and messy, but I also don't give a shit
    plt.style.use('ggplot')
    plt.style.use('seaborn-deep')

    SMALL_SIZE = 8
    MEDIUM_SIZE = 10
    BIGGER_SIZE = 12

    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

    # mpl.rcParams['lines.linewidth'] = 2
    plt.rc('lines', linewidth=1.2)

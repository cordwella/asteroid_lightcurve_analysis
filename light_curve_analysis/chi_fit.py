import pandas as pd
import numpy as np
import scipy.linalg
import pickle


# in JD
# MAX_PERIOD_RESOLUTION = 1 minute =
# 3 month as maximum
MIN_DELTA_F = 1/(4 * 30 * 6)


def phase_function(G, alpha):
    """Compute the Lumme-Bowell G Model adjustment"""
    # half of alpha in radians
    tan_alpha = np.tan(alpha * np.pi / (360))
    phi_1 = np.exp(-3.33 * (tan_alpha)**0.63)
    phi_2 = np.exp(-1.87 * (tan_alpha)**1.22)
    phi = (1 - G) * phi_1 + G * phi_2
    return 2.5 * np.log10(phi)


def generate_chi_squared_grid(data):
    """
    Attempt to fit the lightcurve and generate the chi^2 grid along
    frequency and G

    input:
    data: pandas preprocessed dataset of the data

    (TODO: add in options to this )

    returns:
    a dictionary with values
            {'output_chi_squared': output_chi_squared,
            'output_linear_parameters': output_linear_parameters,
            'output_linear_covarainces': output_linear_covariances,
            'f_values': f_values, 'g_values': g_values}
    """

    red_mag = data['distance_red_mag'].to_numpy()
    adjusted_time = data['adjusted_time'].to_numpy()
    adjusted_time = adjusted_time - adjusted_time[0]

    g_min = -0.3
    g_max = 0.7
    delta_g = 0.005

    g_values = g_min + delta_g * np.arange(int((g_max - g_min)/delta_g))

    # last time point - first time point
    delta_t = adjusted_time[-1] - adjusted_time[0]

    # over sample frequency spacing for both error estimation reasons
    # and also due to the non constant spacing
    delta_f = 1/(4 * delta_t)

    if delta_f < MIN_DELTA_F:
        delta_f = MIN_DELTA_F

    f_max = 1/(1.8/24)  # 2.2 hr, frequency in jd^-1
    f_min = delta_f

    f_values = f_min + delta_f * np.arange(int(f_max/delta_f) + 1)

    # setup the output matricies
    output_chi_squared = np.zeros((len(f_values), len(g_values)))
    output_linear_parameters = [[0 for i in g_values] for f in f_values]
    output_linear_covariances = [[0 for i in g_values] for f in f_values]

    # matrix multiplication to setup reused covariance values
    data_covariance = np.diag((data['skymagerr_fix'].to_numpy())**2)
    inverse_data_covariance = scipy.linalg.inv(data_covariance)  # W

    alpha = data['alpha'].to_numpy()

    for f_i in range(len(f_values)):
        for g_i in range(len(g_values)):
            full_reduced_magnitude = red_mag + phase_function(
                g_values[g_i], alpha)
            # compute lin least squares

            # scipy linear algebra functions are slightly faster

            theta = 2 * np.pi * f_values[f_i] * adjusted_time

            # design matrix
            A = np.array([np.ones(len(adjusted_time)),
                          np.sin(theta), np.cos(theta),
                          np.sin(2 * theta), np.cos(2 * theta)]).transpose()
            # B = (A.T W A)^-1
            # it is both the covariance matrix of x and used in the fit
            B = scipy.linalg.inv(A.transpose() @ inverse_data_covariance @ A)

            # x = (A.T W A)^-1  A.T W y
            x = B @ A.transpose() @ inverse_data_covariance @ full_reduced_magnitude

            residuals = full_reduced_magnitude - A @ x

            chi_squared = residuals.transpose() @ inverse_data_covariance @ residuals

            output_chi_squared[f_i][g_i] = chi_squared

            output_linear_parameters[f_i][g_i] = x
            output_linear_covariances[f_i][g_i] = B

    # degrees of freedom is number of points minus
    dof = len(red_mag) - 7

    return {'output_chi_squared': output_chi_squared,
            'output_linear_parameters': output_linear_parameters,
            'output_linear_covarainces': output_linear_covariances,
            'f_values': f_values, 'g_values': g_values, 'dof': dof}


if __name__ == '__main__':
    asteroid_id = 2011
    filename = '/home/amelia/Documents/minorplanets/backup/export_jul22/{}_out.csv'.format(asteroid_id)
    output_file = 'test_{}.p'.format(asteroid_id)

    data = pre_clean_data(adjust_for_distance(pd.read_csv(filename)))
    update_error_estimate(data)
    data_dict = generate_chi_squared_grid(data)
    print(data_dict)

    pickle.dump(data_dict, open(output_file, "wb"))

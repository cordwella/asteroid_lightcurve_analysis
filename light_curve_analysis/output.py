# Run the full process and then manage the output

# print summary:
# give output of everything

import numpy as np


def print_summary(data_dict, data):

    print("Period: {} h Frequency: {} jd^-1 H: {} mag G:{} \n Amplitude: {:6.4} mag"
          .format(p_u(data_dict['f_uncertainty_limits']), #, lambda x: 24/x),
                  p_u(data_dict['f_uncertainty_limits']),
                  p_u(data_dict['h_uncertainty_limits']),
                  p_u(data_dict['g_uncertainty_limits']),
                  data_dict['amplitude']))

    print("Reduced Chi {:4.5}".format(data_dict['min_red_chi']))

    check_goodness(data_dict, data)


def p_u(tup, formula=None, full=False):
    av, t, b = tup

    if formula is not None:
        av = formula(av)
        t = formula(t)
        b = formula(b)

    if full:
        return "{} +/- {}".format(av, 0.5 * abs(t - b))
    else:
        return "{:6.4} +/- {:6.4}".format(av, 0.5 * abs(t - b))


def r_u(tup, formula=None, full=False):
    av, t, b = tup

    if formula is not None:
        av = formula(av)
        t = formula(t)
        b = formula(b)
    return ",".join([str(av), str(0.5 * abs(t - b))])


def check_goodness(data_dict, data):
    # check chi squared value below 3*dof

    if data_dict.get('bad_f_uncertainty'):
        data_dict['status'] = False
        data_dict['status_detail'] = 'Unable to get frequency uncertainty'
        print("REJECT")
        print('Unable to get frequency uncertainty')
        return False

    if data_dict['min_red_chi'] > 3:
        data_dict['status'] = False
        data_dict['status_detail'] = "Reduced Chi Squared too high"
        print("REJECT")
        print('Reduced Chi Squared greater than 3')
        return False

    average_error = np.average(data['skymagerr_fix'])

    if average_error >= data_dict['amplitude']:
        data_dict['status'] = False
        data_dict['status_detail'] = "Amplitude too low"
        print("REJECT")
        print("Amplitude too low")
        return False


    data_dict['status'] = True
    data_dict['status_detail'] = "Good"
    print("Checks all good")
    return True


# check goodness:
# does the overal fit meet certain requirements

def csv_summary(asteroid_id, data_dict, data):
    # print csv summary

    # asteroid_id, status, status_detail, min_red_chi, period, period uncertainty, frequency,
    # frequency uncertainty
    # H, h uncertainty, G, G uncertainty, amplitude, average_error,
    # fourier coefficents

    average_error = np.average(data['skymagerr_fix'])
    f_b_i, g_b_i = data_dict['f_b_i'], data_dict['g_b_i']

    fourier_coeffs = (data_dict['output_linear_parameters'][f_b_i][g_b_i])

    return ",".join([str(x) for x in [asteroid_id, data_dict['status'],
                     data_dict['status_detail'],
                     data_dict['min_red_chi'],
                     r_u(data_dict['f_uncertainty_limits'], lambda x: 24/x),
                     r_u(data_dict['f_uncertainty_limits']),
                     r_u(data_dict['h_uncertainty_limits']),
                     r_u(data_dict['g_uncertainty_limits']),
                     data_dict['amplitude'], average_error,
                     *fourier_coeffs]])
